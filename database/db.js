const { Sequelize } = require('sequelize');
let db;
const connectDb = async () => {
    db = new Sequelize('nodejs', 'root', 'password', {
        host: 'localhost',
        dialect: 'mysql'
    });
    await db.authenticate().then(() => console.log('connect db success'))
    return db;
}
const getConnection = () => {
    return db;
}
module.exports = {
    connectDb,
    getConnection,
}
// const { Sequelize } = require('sequelize');
// let db;
// const connectDb = async () => {
//     db = new Sequelize('nodejs', 'root', 'password', {
//         host: 'localhost',
//         dialect: 'mysql'
//     });
//     await db.authenticate().then(() => console.log('connect db success'))
//     return db;
// }
// const getConnection = () => {
//     return db;
// }
// module.exports = {
//     connectDb,
//     getConnection,
// }