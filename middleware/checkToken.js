// const jwt = require('jsonwebtoken');
// require('dotenv').config();
// module.exports = {
//     checkToken: (req, res, next) => {
//         let authorize_token = req.get('Authorization')
//         let jwt_token = ""
//         if (!authorize_token) {
//             res.status(500).send({ message: "json jwt not defined" })
//             return
//         }
//         else if (authorize_token.startsWith("Bearer ")) {
//             jwt_token = authorize_token.substring(7)
//         } else {
//             res.status(400).send("Not exits Bearer")
//             return
//         }
//         try {
//             jwt.verify(jwt_token, process.env.ENV_KEY, (err, decode) => {
//                 if (err) {
//                     throw "invalid token"
//                 }
//             })
//             req.token = jwt_token
//             next()
//         } catch (error) {
//             console.log(error)
//             res.status(400).send("Invalid Token")
//         }
//     }
// }

const jwt = require('jsonwebtoken');
require('dotenv').config();
module.exports = {
    checkToken: (req, res, next) => {
        let authorize_token = req.get('Authorization')
        let jwt_token = ""
        if (!authorize_token) {
            res.status(500).send({ message: "json jwt not defined" })
            return
        }
        else if (authorize_token.startsWith("Bearer ")) {
            jwt_token = authorize_token.substring(7)
        } else {
            res.status(400).send("Not exits Bearer")
            return
        }
        try {
            jwt.verify(jwt_token, process.env.ENV_KEY, (err, decode) => {
                if (err) {
                    throw "invalid token"
                }
            })
            req.token = jwt_token
            next()
        } catch (error) {
            console.log(error)
            res.status(400).send("Invalid Token")
        }
    }
}


//             })
//             req.token = jwt_token
//             next()
//         } catch (error) {
//             console.log(error)
//             res.status(400).send("Invalid Token")
//         }
//     }
// }
