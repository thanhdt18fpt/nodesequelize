const callModels = require('../../models/accounts/index')

const fecthAll = async () => {
    let result;
    result = await callModels.fetchAll()
    return result
}

const addAccount = async (name, email) => {
    let result;
    result = await callModels.addAccount(name, email)
    return result
}

const isExitsWithEmail = async (email) => {
    let result;
    result = await callModels.isExitsWithEmail(email)
    return result
}

const updateById = async (id, name, email) => {
    let result;
    result = await callModels.updateById(id, name, email)
    return result
}

const deleteById = async (id, name, email) => {
    let result;
    result = await callModels.deleteById(id, name, email)
    return result
}

module.exports = { fecthAll, addAccount, isExitsWithEmail, deleteById, updateById }