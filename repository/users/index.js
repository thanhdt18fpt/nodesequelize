const callModels = require('../../models/users/index')

const findByEmail = async (email) => {
    let result;
    result = await callModels.findByEmail(email)
    return result
}

const register = async (user) => {
    return await callModels.register(user)
}

module.exports = { findByEmail, register }