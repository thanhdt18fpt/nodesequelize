const accountRouter = require('./routes/accounts');
const usersRouter = require('./routes/users');
const express = require('express');
const bodyParser = require("body-parser");
const { handleError } = require('./middleware/handleException');
class App {
    constructor() {
    }

    static listen() {
        var app = express();
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
        app.listen(3000, () => { console.log('runing') })
        app.use('/accounts', accountRouter)
        app.use('/users', usersRouter)

        // xu ly kiem tra neu khong co path nao mapping throw 
        app.all('*', (req, res, next) => {
            const err = new Error('The route not found');
            err.statusCode = 404;
            next(err)
        })
        app.use(handleError)
    }

}

module.exports = App