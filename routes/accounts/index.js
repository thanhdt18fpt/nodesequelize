const express = require('express');
const router = express.Router();
const accController = require('../../controllers/accounts')
const { checkToken } = require('../../middleware/checkToken')

// get all accounts
router.get('/', checkToken, accController.fecthAll);
router.post('/add', checkToken, accController.addAccount);
router.post('/getbyemail', checkToken, accController.isExitsWithEmail);
router.patch('/:id', checkToken, accController.updateById);
router.delete('/:id', checkToken, accController.deleteById);
module.exports = router;
