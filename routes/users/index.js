const express = require('express');
const router = express.Router();
const usersController = require('../../controllers/users')

// get all accounts
router.post('/check', usersController.findByEmail);
router.post('/register', usersController.register);
router.post('/login', usersController.login);
module.exports = router;
