const { connectDb } = require('./database/db')

const running = async () => {
    const x = await connectDb()

    // chỗ này k đc lazy -> chưa đc đinh nghĩa 
    const app = require('./app')
    app.listen()
}
running()