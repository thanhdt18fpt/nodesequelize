const accRep = require('../../repository/accounts/index')
const fecthAll = async () => {
    return await accRep.fecthAll()
}

const addAccount = async (name, email) => {
    if (name && email) {
        return await accRep.addAccount(name, email)
    }
}

const isExitsWithEmail = async (email) => {
    return await accRep.isExitsWithEmail(email)
}

const updateById = async (id, name, email) => {
    let result;
    result = await accRep.updateById(id, name, email)
    return result
}
const deleteById = async (id) => {
    let result;
    result = await accRep.deleteById(id)
    return result
}

module.exports = { fecthAll, addAccount, isExitsWithEmail, deleteById, updateById }