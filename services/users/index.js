const callRepository = require('../../repository/users/index')
const jwt = require('jsonwebtoken');
require('dotenv').config();
const findByEmail = async (email) => {
    let result;
    result = await callRepository.findByEmail(email)
    return result
}

const register = async (user) => {
    let userExits = await findByEmail(user.email)
    if (userExits) {
        throw new Error('Username "' + user.email + '" is already taken');
    }
    await callRepository.register(user)
    // create a jwt token that is valid for 7 days
    const token = jwt.sign({ sub: user.id }, process.env.ENV_KEY, { expiresIn: '7d' });

    return {
        ...omitPassword(user),
        token
    };
}

const omitPassword = (user) => {
    const { password, ...userWithoutPassword } = user;
    return userWithoutPassword;
}

const login = async (user) => {
    let userExits = await findByEmail(user.email)
    let payload = {
        email: user.email,
        type: 'access'
    }
    if (!userExits || user.password !== userExits.password) {
        throw new Error('Email or Password is not valid');
    }
    let token = jwt.sign(payload, process.env.ENV_KEY, { expiresIn: '7d' })
    return token
}

module.exports = { findByEmail, register, login }