const callService = require('../../services/users/index')

exports.findByEmail = async (req, res, next) => {
    if (!req.body.email) {
        res.status(400).send({
            message: "email can not be empty!"
        });
    }
    let result = await callService.findByEmail(email)
    res.send(result)
}


exports.register = async (req, res, next) => {
    let user = {
        email: req.body.email,
        userName: req.body.userName,
        password: req.body.password,
        address: req.body.address
    }
    if (!user.email || !user.userName || !user.password || !user.address) {
        const err = new Error("Invalid Data Register")
        err.statusCode = 400;
        return next(err)
    }
    if (!validateEmail(user.email)) {
        const err = new Error('email is wrong');
        err.statusCode = 400;
        return next(err)
    }
    try {
        let result = await callService.register(user)
        res.send(result)
    } catch (err) {
        return next(err)
    }

}

exports.login = async (req, res, next) => {
    let user = req.body
    if (!validateEmail(user.email)) {
        const err = new Error('email is wrong');
        err.statusCode = 400;
        return next(err)
    }
    try {
        let result = await callService.login(user)
        res.send(result)
    } catch (err) {
        return next(err)
    }
}

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}