const accService = require('../../services/accounts/index')

exports.fecthAll = async (req, res) => {
    let result = await accService.fecthAll()
    res.send(result)
}

exports.addAccount = async (req, res, next) => {
    let email = req.body.email;
    let name = req.body.name;
    if (email && name) {
        try {
            let result = await accService.addAccount(name, email)
            res.send(result)
        } catch {
            const err = new Error("Error")
            err.statusCode = 400;
            return next(err)
        }
    } else {
        const err = new Error("Name Or Email wrong !!!")
        err.statusCode = 400;
        return next(err)
    }
}

exports.isExitsWithEmail = async (req, res, next) => {
    let email = req.body.email;
    if (!email) {
        res.status(400).send({
            message: "email can not be empty!"
        });
    }

    let result = await accService.isExitsWithEmail(email)
    res.send(result)
}

exports.updateById = async (req, res, next) => {
    let id = req.params.id;
    let email = req.body.email;
    let name = req.body.name;
    console.log(email, name, 'controllers ')
    result = await accService.updateById(id, name, email)
    res.send(result)
}

exports.deleteById = async (req, res, next) => {
    let id = req.params.id;
    result = await accService.deleteById(id)
    res.send({ message: 'delete success !!!' })
}