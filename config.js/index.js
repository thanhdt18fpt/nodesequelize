module.exports = {
    HOST: process.env.HOST,
    USER: process.env.ENV_USER_DB,
    PASSWORD: process.env.ENV_USER_DB_PASSWORD,
    DB: process.env.ENV_DB,
    dialect: process.env.ENV_DIALET,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};