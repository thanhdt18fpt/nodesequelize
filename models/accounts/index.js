const { QueryTypes, DataTypes, Model } = require('sequelize');
const { getConnection } = require('../../database/db')
class Accounts extends Model { }

Accounts.init({
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    sequelize: getConnection(),
    modelName: 'accounts'
});

Accounts.sync()

module.exports = class Account {
    constructor() {

    }
    static async fetchAll() {
        let accounts = await Accounts.findAll();
        return accounts
    }
    static async addAccount(name, email) {
        let newAccount = await Accounts.create({ name, email });
        return newAccount
    }

    static async isExitsWithEmail(email) {
        let account = await Accounts.findOne({ where: { email } });
        return account
    }

    static async updateById(id, name, email) {
        // let account = await Accounts.update({ name, email }, {
        //     where: {
        //         id
        //     }
        let nameLike = name.concat('%')
        let account = await getConnection().query(
            'SELECT * FROM accounts WHERE name  LIKE :name ORDER BY name ASC ',
            {
                replacements: { name: nameLike },
                type: QueryTypes.SELECT
            }
        );
        return account
    }

    static async deleteById(id) {
        let account = await Accounts.destroy({ where: { id } });
        return account
    }
}