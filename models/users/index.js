const { QueryTypes, DataTypes, Model } = require('sequelize');
const { getConnection } = require('../../database/db')
class Users extends Model { }

Users.init({
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    userName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false
    },
}, {
    sequelize: getConnection(),
    modelName: 'users'
});

Users.sync()

module.exports = class User {
    constructor() {
    }
    static async findByEmail(email) {
        let user = await Users.findOne({ where: { email }, raw: true });
        return user
    }
    static async register(user) {
        return await Users.create({ ...user })
    }
}